provider "aws" {
  region = "us-east-1"
}

terraform {
    backend "s3" {
        bucket = "cakecore-dev-terraform-state"
        key = "tst1-ecs-sales-api.tfstate"
        region = "us-east-1"
        encrypt = true
    }
}
data "aws_security_group" "cluster_sg" {
  name = "${var.security_group_name}"
}

data "aws_lb" "cluster_alb" {
  name = "${var.alb_name}"
}

data "aws_lb_listener" "selected_listener" {
  load_balancer_arn = "${data.aws_lb.cluster_alb.arn}"
  port              = "${var.alb_listener_port}"
}


resource "aws_alb_target_group" "tst1-ecs-sales-api" {
    name                = "tst1-ecs-sales-api"
    port                = "80"
    protocol            = "HTTP"
    vpc_id              = "${var.vpc_id}"

    health_check {
        healthy_threshold   = "5"
        unhealthy_threshold = "2"
        interval            = "30"
        matcher             = "200"
        path                = "${var.alb_healthcheck}"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = "5"
    }

    tags {
      Name = "tst1-ecs-sales-api"
    }
}

#resource "aws_alb_listener" "sales-api-listener" {
#    load_balancer_arn = "${aws_alb.tst1-ecs-sales-api-alb.arn}"
#    port              = "${var.alb_listener_port}"
#    protocol          = "HTTP"
#
#    default_action {
#        target_group_arn = "${aws_alb_target_group.tst1-ecs-sales-api.arn}"
#        type             = "forward"
#    }
#}

resource "aws_lb_listener_rule" "host_based_routing" {
  listener_arn = "${data.aws_lb_listener.selected_listener.arn}"
  priority     = 590

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.tst1-ecs-sales-api.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${var.app_dns}"]
  }
} 

data "aws_ecs_task_definition" "task_family" {
  task_definition = "${var.task_definition_id}"
}


resource "aws_ecs_service" "sales-api_service" {
  lifecycle {
    create_before_destroy = true
  }

  name                               = "${var.service_name}"
  cluster                            = "${var.cluster_name}"
  task_definition                    = "${var.task_definition_id}:${data.aws_ecs_task_definition.task_family.revision}"
  desired_count                      = "${var.desired_count}"
  deployment_minimum_healthy_percent = "${var.deployment_min_healthy_percent}"
  deployment_maximum_percent         = "${var.deployment_max_percent}"
  iam_role                           = "${var.ecs_service_role_name}"

  ordered_placement_strategy {
    field = "attribute:ecs.availability-zone"
    type  = "spread"
  }

  ordered_placement_strategy {
    field = "instanceId"
    type  = "spread"
  }

   depends_on = [
    "aws_lb_listener_rule.host_based_routing",
  ]

  load_balancer {
    target_group_arn = "${aws_alb_target_group.tst1-ecs-sales-api.id}"
    container_name   = "${var.container_name}"
    container_port   = "${var.container_port}"
  }
}


data "aws_iam_role" "ecs_service_role" {
  name = "${var.ecs_service_role_name}"
}


resource "aws_appautoscaling_target" "replicas" {
  max_capacity       = "${var.max_replicas}"
  min_capacity       = 1
  resource_id        = "service/${var.cluster_name}/${aws_ecs_service.sales-api_service.name}"
  role_arn           = "${data.aws_iam_role.ecs_service_role.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "replicas" {
  name               = "cpu-auto-scaling"
  service_namespace  = "${aws_appautoscaling_target.replicas.service_namespace}"
  scalable_dimension = "${aws_appautoscaling_target.replicas.scalable_dimension}"
  resource_id        = "${aws_appautoscaling_target.replicas.resource_id}"
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value = 90
    scale_in_cooldown = 300
    scale_out_cooldown = 300
  }
}
