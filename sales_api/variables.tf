# main creds for AWS connection
variable "cluster_name" {
  description = "ECS cluster name"
}

variable "region" {
  description = "AWS region"
}

########################### Test VPC Config ################################

variable "vpc_id" {
  description = "VPC ID for Test environment"
}
variable "alb_name" {
  description = "ALB name for the environment"
}
variable "container_port" {
  description = "ALB name for the environment"
}
variable "desired_count" {
  description = "desired_count for the environment"
}
variable "task_definition_id" {
  description = "task_definition_id for the environment"
}
variable "deployment_min_healthy_percent" {
  description = "deployment_min_healthy_percent for the environment"
}
variable "deployment_max_percent" {
  description = "deployment_max_percent for the environment"
}
variable "ecs_service_role_name" {
  description = "ecs_service_role_name for the environment"
}
variable "container_name" {
  description = "container_name for the environment"
}

variable "alb_listener_port" {
  description = "ALB listner for the environment"
}

variable "alb_healthcheck" {
  description = "Health check for the environment"
}

variable "security_group_name" {
  description = "security_group_name for the environment"
}

variable "max_replicas" {
  description = "max_replicas for the environment"
}

#variable "ecs_subnet_01" {
#  description = "Externally accessible subnet"
#}
#
#variable "ecs_subnet_02" {
#  description = "Externally accessible subnet"
#}

variable "service_name" {
  description = "service_name for the ECS Service"
}



variable "app_dns" {
  description = "DNS for the ECS Service"
}